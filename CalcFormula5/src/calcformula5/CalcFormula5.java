
package calcformula5;

import java.util.Scanner; 

public class CalcFormula5 {

    
    public static void main(String[] args) {
        System.out.println("Введите сторону ромба: ");
        double a = 0; 
        Scanner myInput = new Scanner(System.in); 
        a = myInput.nextDouble();
        
        System.out.println("Введите острый угол ромба: ");
        double b = 0; 
        b = myInput.nextDouble();
        double c = Math.sin(b); 
        
        System.out.println("Площадь ромба равна: " + a*a*c);
    }
    
}
