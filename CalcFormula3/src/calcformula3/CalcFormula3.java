
package calcformula3;

import java.util.Scanner;

public class CalcFormula3 {

   
    public static void main(String[] args) {
        System.out.println("Введите радиус окружности: ");
        double r = 0;
        Scanner myInput = new Scanner (System.in);
        r = myInput.nextDouble();
        double p = 2; 
        
        double r2 = Math.pow(r,p); 
        double result = r2 * 3.145; 
        
        System.out.println("Площадь окружности равна: " + result);
    }
    
}
