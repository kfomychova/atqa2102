
package cycle18;


public class Cycle18 {

    
    public static void main(String[] args) {
        int n = 44567; 
        int m = 0;
        while ( n > 0 )
        {
            m = n % 10; 
            System.out.print(m + " ");
            n = n / 10;
        }
    }
    
}
