
package calcformula4;

import java.util.Scanner; 

public class CalcFormula4 {

    
    public static void main(String[] args) {
        System.out.println("Введите радиус окружности: ");
        double r = 0; 
        Scanner myInput = new Scanner(System.in); 
        r = myInput.nextDouble();
        
        System.out.println("Длинна окружности равна: " + 2* 3.145 * r);
        
    }
    
}
