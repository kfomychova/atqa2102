
package cycles.string;


public class CyclesString {

    public static void main(String[] args) {
        // вывести цыфры числа в обратном порядке
        // 324 --> 423
        // определить кол-во цыфр в числе
        // определить есть ли в строке символ 7 
        // посчитать кол-во пробелов
        
                   // 0 1 2
        String str1 = "324";
        System.out.println("строка =  "+ str1);
       System.out.println( "символномер 2 в ней это " + str1.charAt(2));
        // определить кол-во цыфр в числе
        int n = str1.length(); // кол-во символов
        
        // 324 --> 423
        // номер последней буквы это кол-во символов минус 1
        System.out.println("Перевёрнутая строка = ");
        for (int i = n -1 ; i >=0 ; i--)
        {
        System.out.println(str1.charAt(i));
        }
        
        
        // определить есть ли в строке символ 7 
        
        boolean state = false; 
        char symbol = '7';
        for (int i = 0; i < str1.length(); i++)
        {
        if (str1.charAt(i) == symbol ) // одинарные кавычки означают одну букву
        {
            System.out.println("Символ 7 входит") ;
            state = true; 
            break;
        }
        }
        if (state == false)
        {
         System.out.println("Символ 7 не входит");
        }
        
        
        // посчитать кол-во пробелов
        
        boolean stateS = false; 
        char symbolNone = ' ';
        for (int i = 0; i < str1.length(); i++)
        {
        if (str1.charAt(i) == symbolNone ) // одинарные кавычки означают одну букву
        {
            System.out.println("Символ пробел входит") ;
            stateS = true; 
            break;
        }
        }
        if (stateS == false)
        {
         System.out.println("Символ пробел не входит");
        }
    }
    
}
