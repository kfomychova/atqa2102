
package cycle19;


public class Cycle19 {

    
    public static void main(String[] args) {
         int n = 333; 
        int m = 0;
        int sum = 0; 
        int count = 0; 
        while ( n > 0 )
        {
            m = n % 10; 
            sum = sum + m;
            n = n / 10;
            count++; 
        }
        System.out.println("Сумма числа равна "+sum);
        System.out.println("Количество цыфр равно "+ count);
    }
    
}
