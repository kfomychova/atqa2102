
package calcformula2;

import java.util.Scanner;

public class CalcFormula2 {

    
    public static void main(String[] args) {
        System.out.println("Введите площадь пирамиды: ");
        double S = 0;
        Scanner myInput = new Scanner (System.in);
        S = myInput.nextDouble();
        
        System.out.println("Введите высоту пирамиды: ");
        double H = 0;
        H = myInput.nextDouble();
        
        System.out.println("Объём пирамиды равен: " + (S*H)/3);
        
    }
    
}
